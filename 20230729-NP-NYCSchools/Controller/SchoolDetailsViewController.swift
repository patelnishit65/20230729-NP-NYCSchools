//
//  SchoolDetailsViewController.swift
//  
// 
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation
import UIKit

class SchoolDetailsViewController: UIViewController {
    
    static func instantiate(schoolDetails: SchoolDetails) -> SchoolDetailsViewController {
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Self.className) as! SchoolDetailsViewController
        vc.schoolDetails = schoolDetails
        return vc
    }
    
    //
    // MARK: - Outlets
    //
    
    @IBOutlet private weak var labelSchoolName: UILabel!
    
    @IBOutlet private weak var labelCriticalScore: UILabel!
    @IBOutlet private weak var labelMathsScore: UILabel!
    @IBOutlet private weak var labelWritingScore: UILabel!
    
    @IBOutlet private weak var labelOverView: UILabel!
    @IBOutlet private weak var labelAddress: UILabel!
    @IBOutlet private weak var labelTelephone: UILabel!
    @IBOutlet private weak var labelWebsite: UILabel!
    
    //
    // MARK: - Variables
    //
    
    private var schoolDetails: SchoolDetails?
    
    //
    // MARK: - View Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpLabels()
    }
    
    //
    // MARK: - Methods
    //
    
    /// The function sets up all labels on the screen
    func setUpLabels() {
        
        guard let schoolDetails = schoolDetails else {
            return }
        
        labelSchoolName.text = schoolDetails.name
        
        labelCriticalScore.text = "Critical Reading: \(schoolDetails.readingScore)"
        labelWritingScore.text = "Writing Average: \(schoolDetails.writingScore)"
        labelMathsScore.text = "Math Average: \(schoolDetails.mathScore)"
        
        labelOverView.text = schoolDetails.overviewParagraph
        
        labelAddress.attributedText = "Address: \(schoolDetails.address)".attributedText(boldString: "Address:")
        
        labelTelephone.attributedText = "Telephone: \(schoolDetails.telephoneNumber)".attributedText(boldString: "Telephone:")
        
        labelWebsite.attributedText = "Website: \(schoolDetails.website)".attributedText(boldString: "Website:")
        
    }
}
