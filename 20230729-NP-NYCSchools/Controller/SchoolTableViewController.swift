//
//  SchoolTableViewController.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import UIKit
import MapKit
import MessageUI

class SchoolTableViewController: UIViewController {
    
    //
    // MARK: - Variables
    //
    
    private var schoolListViewModel: SchoolListViewModel!
    private var schoolDetailsViewModel: SchoolDetailsViewModel!
    
    //
    // MARK: - Outlets
    //
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyStateView: UIView!
    @IBOutlet private weak var activitySpinner: UIActivityIndicatorView!
    
    //
    // MARK: - View Lifecycle
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showEmptyStateView()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.schoolListViewModel = SchoolListViewModel()
        self.schoolListViewModel.bindSchoolListViewModelToController = {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
                self?.showTableView()
            }
        }
        
        self.schoolDetailsViewModel = SchoolDetailsViewModel()
    }
    //
    // MARK: - Methods
    //
    
    func showEmptyStateView() {
        self.tableView.isHidden = true
        self.emptyStateView.isHidden = false
        self.activitySpinner.isHidden = false
        self.activitySpinner.startAnimating()
    }
    
    func showTableView() {
        self.tableView.isHidden = false
        self.activitySpinner.isHidden = true
        self.emptyStateView.isHidden = true
        self.activitySpinner.stopAnimating()
    }
}


// MARK: - TableView
extension SchoolTableViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolListViewModel.arraySchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolTableViewCell.className, for: indexPath) as? SchoolTableViewCell else {
            return UITableViewCell()
        }
        
        let school = self.schoolListViewModel.arraySchools[indexPath.row]
        cell.school = school
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        guard let cell = tableView.cellForRow(at: indexPath) as? SchoolTableViewCell else {
            return
        }
        
        cell.tapAnimation(completion: { [weak self] in
            
            guard let self = self else { return }
            
            let school = self.schoolListViewModel.arraySchools[indexPath.row]
            
            guard var schoolDetails = schoolDetailsViewModel.getSchoolDetail(for: school.dbn) else {
                
                self.showErrorAlert(message: "School details are not available.")
                 return
            }
            
            schoolDetails.name = school.name
            schoolDetails.address = school.address
            schoolDetails.overviewParagraph = school.overviewParagraph
            schoolDetails.website = school.website
            schoolDetails.telephoneNumber = school.phone
            
            let schoolDetailVC = SchoolDetailsViewController.instantiate(schoolDetails: schoolDetails)
            self.navigationController?.pushViewController(schoolDetailVC,
                                                          animated: true)
        })
        
    }
}

// MARK: - SchoolTableViewCellDelegate
extension SchoolTableViewController: SchoolTableViewCellDelegate
{
    func didTapButtonCall(_ cell: SchoolTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        let schoolPhoneNumber = self.schoolListViewModel.arraySchools[indexPath.row].phone
        
        if let url = URL(string: "tel://\(String(describing: schoolPhoneNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            
            self.showErrorAlert(message: "Calling functionality is not available  \(schoolPhoneNumber)")
        }
    }
    
    func didTapButtonAddress(_ cell: SchoolTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        let school = self.schoolListViewModel.arraySchools[indexPath.row]
        let latitude = school.latitude
        let longitude = school.longitude
        let name = school.name
        
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = name
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        
    }
    
    func didTapButtonEmail(_ cell: SchoolTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        let email = self.schoolListViewModel.arraySchools[indexPath.row].email
        
        if MFMailComposeViewController.canSendMail() {
                let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
                mail.setToRecipients([email])
                present(mail, animated: true)
            } else {
                
                self.showErrorAlert(message: "Mail app is not available")
            }
    }
    
    func didTapButtonWebsite(_ cell: SchoolTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        
        var schoolURL = self.schoolListViewModel.arraySchools[indexPath.row].website
        if schoolURL.hasSuffix("https://") == false
        {
            let tempURL = schoolURL
            schoolURL = "https://"
            schoolURL += tempURL
        }
        
        guard let url = URL(string: schoolURL) else {
          return //be safe
        }

        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}

extension SchoolTableViewController: MFMailComposeViewControllerDelegate
{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
