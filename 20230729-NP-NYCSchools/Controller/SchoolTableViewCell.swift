//
//  SchoolTableViewCell.swift
//
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import UIKit

protocol SchoolTableViewCellDelegate: AnyObject
{
    func didTapButtonCall(_ cell: SchoolTableViewCell)
    func didTapButtonAddress(_ cell: SchoolTableViewCell)
    func didTapButtonEmail(_ cell: SchoolTableViewCell)
    func didTapButtonWebsite(_ cell: SchoolTableViewCell)
}

class SchoolTableViewCell: UITableViewCell {
    
    //
    // MARK: - Variables
    //
    
    var school: SchoolData? {
        didSet {
            self.setSchoolDetails()
        }
    }
    
    weak var delegate: SchoolTableViewCellDelegate?
    
    //
    // MARK: - Outlets
    //
    @IBOutlet private weak var viewContent: UIView!
    
    @IBOutlet private weak var labelName: UILabel!
    @IBOutlet private weak var labelAddress: UILabel!
    @IBOutlet private weak var labelInterest: UILabel!
    
    @IBOutlet private weak var buttonCall: UIButton!
    @IBOutlet private weak var buttonAddress: UIButton!
    @IBOutlet private weak var buttonEmail: UIButton!
    @IBOutlet private weak var buttonWebsite: UIButton!
    
    
    //
    // MARK: - Methods
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.buttonCall.addTarget(self, action: #selector(didTapButtonCall(_:)), for: .touchUpInside)
        self.buttonAddress.addTarget(self, action: #selector(didTapButtonAddress(_:)), for: .touchUpInside)
        self.buttonEmail.addTarget(self, action: #selector(didTapButtonEmail(_:)), for: .touchUpInside)
        self.buttonWebsite.addTarget(self, action: #selector(didTapButtonWebsite(_:)), for: .touchUpInside)
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        for button in [
            buttonCall,
            buttonAddress,
            buttonEmail,
            buttonWebsite
        ]
        {
            button?.makeCircleView()
        }
        
        setupCardViewShadows()
        
    }
    
    private func setSchoolDetails() {
        
        guard let school = school else { return }
        
        labelName.text = school.name
        labelAddress.attributedText = "Address: \(school.address)".attributedText(boldString: "Address:")
        
        labelInterest.attributedText = "Area of Interest: \(school.interest1)".attributedText(boldString: "Area of Interest:")
        
        self.buttonAddress.isHidden = school.address.isEmpty
        self.buttonCall.isHidden = school.phone.isEmpty
        self.buttonWebsite.isHidden = school.website.isEmpty
        self.buttonEmail.isHidden = school.email.isEmpty
    }
    
    private func setupCardViewShadows(){
        viewContent?.layer.cornerRadius = 15.0
        viewContent?.layer.shadowColor = UIColor.systemGray2.cgColor
        viewContent?.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewContent?.layer.shadowOpacity = 0.8
        viewContent?.layer.shadowRadius = 5
        viewContent?.layer.masksToBounds = false
    }
}


//MARK: Button Actions
extension SchoolTableViewCell: SchoolTableViewCellDelegate
{
    @objc internal func didTapButtonCall(_ cell: SchoolTableViewCell) {
        
        buttonCall.tapAnimation(completion: { [weak self] in
            guard let self = self else { return }
            delegate?.didTapButtonCall(self)
        })
    }
    
    @objc internal func didTapButtonAddress(_ cell: SchoolTableViewCell) {
        buttonAddress.tapAnimation(completion: { [weak self] in
            guard let self = self else { return }
            delegate?.didTapButtonAddress(self)
        })
    }
    
    @objc internal func didTapButtonEmail(_ cell: SchoolTableViewCell) {
        buttonEmail.tapAnimation(completion: { [weak self] in
            guard let self = self else { return }
            delegate?.didTapButtonEmail(self)
        })
    }
    
    @objc internal func didTapButtonWebsite(_ cell: SchoolTableViewCell) {
        buttonWebsite.tapAnimation(completion: { [weak self] in
            guard let self = self else { return }
            delegate?.didTapButtonWebsite(self)
        })
    }
}
