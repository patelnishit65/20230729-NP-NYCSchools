//
//  Keys.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation
import UIKit

class Keys {
    static let schoolCell = "schoolCell"
    static let toSchoolDetailsVC = "toSchoolDetailsVC"
    static let salmon = UIColor(red: 253.0/255.0, green: 127.0/255.0, blue: 124.0/255.0, alpha: 1.0)
    static let customBlue = UIColor(red: 148.0/255.0, green: 195.0/255.0, blue: 245.0/255.0, alpha: 1.0)
    static let detailTextColor = UIColor.white
    static let mainTextColor = UIColor.darkText
}


struct URLs {
    static let schoolList = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    static let schoolDetails = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
}
