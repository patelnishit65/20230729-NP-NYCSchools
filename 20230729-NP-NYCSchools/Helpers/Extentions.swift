//
//  Extentions.swift
//
//
//  Created by NKP on 7/29/23.
//  Copyright © 2023 NKP. All rights reserved.
//

import UIKit

//MARK: NSObject
extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}

//MARK: UIView
extension UIView {
    func makeCircleView() {
        let radius = min(self.frame.size.width, self.frame.size.height)
        self.roundedCorner(radius/2)
    }
    
    func roundedCorner(_ value: CGFloat = 3) {
        
        var radius = value
        if self.frame.size.height != 0
        {
            // If frame is not equal to zero, we need to apply minimum Rounded corner from 2 factors.
            radius = min(value, self.frame.size.height/2)
        }
        
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func tapAnimation(completion: (() -> ())? = nil){
        
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
            self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { finished in
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                
                self.transform = CGAffineTransform.identity
            }) { _ in
                
                completion?()
            }
        }
        
    }
}

//MARK: UIViewController
extension UIViewController
{
    func showErrorAlert(message: String)
    {
        let alertView = UIAlertController(title: "Error!",
                                          message: message,
                                          preferredStyle: .alert)
        
        let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        
        alertView.addAction(okayAction)
        
        self.present(alertView, animated: true, completion: nil)
    }
}

//MARK: String
extension String
{
    func attributedText(boldString: String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: self,
                                                         attributes: [.font: UIFont.systemFont(ofSize: 15)])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 15),
                                                                .foregroundColor: UIColor.label]
        let range = (self as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
}
