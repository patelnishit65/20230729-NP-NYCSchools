//
//  School.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation

struct SchoolData: Decodable {
    
    //
    // MARK: - Variables
    //
    
    let name: String
    let totalStudents: String
    let dbn: String
    let address: String
    private let location: String
    let interest1: String
    let email: String
    let website: String
    let phone: String
    let latitude: Double
    let longitude: Double
    let overviewParagraph: String
    
    //
    // MARK: - Coding Keys
    //
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case totalStudents = "total_students"
        case dbn
        case address
        case location
        case interest1
        case email = "school_email"
        case website
        case phone = "phone_number"
        case longitude
        case lattitude
        case overviewParagraph = "overview_paragraph"
    }
    
    //
    // MARK: - Initalizer
    //
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.totalStudents = try values.decodeIfPresent(String.self, forKey: .totalStudents) ?? ""
        self.dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        
        self.location = try values.decodeIfPresent(String.self, forKey: .location) ?? ""
        self.address = location.components(separatedBy: "(").first ?? ""
        self.interest1 = try values.decodeIfPresent(String.self, forKey: .interest1) ?? ""
        self.email = try values.decodeIfPresent(String.self, forKey: .email) ?? ""
        self.website = try values.decodeIfPresent(String.self, forKey: .website) ?? ""
        self.phone = try values.decodeIfPresent(String.self, forKey: .phone) ?? ""
        self.overviewParagraph = try values.decodeIfPresent(String.self, forKey: .overviewParagraph) ?? ""
        
        let coordinate = (self.location.components(separatedBy: "(").last ?? "").components(separatedBy: ",")
        
        if let lat = coordinate.first
        {
            self.latitude = Double(lat) ?? 0.0
        }
        else
        {
            self.latitude = 0.0
        }
        
        if coordinate.count == 2
        {
            var long = coordinate[1]
            long = long.replacingOccurrences(of: " ", with: "")
            long = long.replacingOccurrences(of: ")", with: "")
            self.longitude = Double(long) ?? 0.0
        }
        else
        {
            self.longitude = 0.0
        }
    }
}
