//
//  SchoolDetails.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation

struct SchoolDetails: Decodable {
    
    //
    // MARK: - Variables
    //
    
    var name: String
    let numberOfTestTakers: String
    let readingScore: String
    let mathScore: String
    let writingScore: String
    let dbn: String
    
    var overviewParagraph: String
    var address: String
    var website:String
    var telephoneNumber: String
    
    
    
    //
    // MARK: - Coding Keys
    //
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
        case dbn
    }
    
    //
    // MARK: - Initializer
    //
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.numberOfTestTakers = try values.decodeIfPresent(String.self, forKey: .numberOfTestTakers) ?? ""
        self.readingScore = try values.decodeIfPresent(String.self, forKey: .readingScore) ?? ""
        self.mathScore = try values.decodeIfPresent(String.self, forKey: .writingScore) ?? ""
        self.writingScore = try values.decodeIfPresent(String.self, forKey: .mathScore) ?? ""
        self.dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        
        self.overviewParagraph = ""
        self.address = ""
        self.website = ""
        self.telephoneNumber = ""
    }
}
