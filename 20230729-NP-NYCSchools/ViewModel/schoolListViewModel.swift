//
//  SchoolController.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation

class SchoolListViewModel: NSObject {
    
    //
    // MARK: - Variables
    //
    
    private var apiService : NetworkService!
    private(set) var arraySchools : [SchoolData] = [] {
        didSet {
            self.bindSchoolListViewModelToController()
        }
    }
    
    var bindSchoolListViewModelToController : (() -> ()) = {}
    
    //
    // MARK: - Methods
    //
    
    override init() {
        super.init()
        self.apiService =  NetworkService()
        self.fetchSchools()
    }
    
    private func fetchSchools() {
        
        apiService.triggerService(url: URLs.schoolList,
                                     resultType: [SchoolData].self,
                                     completion: { response in
            switch response {
            case .success(let schools):
                self.arraySchools = schools
            case .failure(_):
                self.arraySchools = []
            }
        })
    }
}
