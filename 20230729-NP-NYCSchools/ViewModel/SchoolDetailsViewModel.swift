//
//  SchoolDetailsController.swift
//  
//
//  Created by NKP on 7/29/2023.
//  Copyright © 2023 NKP. All rights reserved.
//

import Foundation

class SchoolDetailsViewModel: NSObject {
    
    private var apiService : NetworkService!
    private(set) var arraySchoolDetails : [SchoolDetails] = []
    
    var bindSchoolListViewModelToController : (() -> ()) = {}
    
    //
    // MARK: - Methods
    //
    
    override init() {
        super.init()
        self.apiService =  NetworkService()
        self.fetchSchoolDetails()
    }
    
    private func fetchSchoolDetails() {
        
        apiService.triggerService(url: URLs.schoolDetails,
                                 resultType: [SchoolDetails].self,
                                 completion: { response in
            switch response {
            case .success(let schoolDetails):
                self.arraySchoolDetails = schoolDetails
            case .failure(_):
                self.arraySchoolDetails = []
            }
        })
    }
    
    ///  This funcrion will return a single school object.
    /// - Parameter schoolBDN: provide requested school's DBN number
    func getSchoolDetail(for schoolDBN: String) -> SchoolDetails?
    {
        if arraySchoolDetails.isEmpty
        {
            self.fetchSchoolDetails()
            return nil
        }
        return arraySchoolDetails.first(where: {
            $0.dbn == schoolDBN
        })
    }
    
}
